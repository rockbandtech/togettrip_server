class CreateSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :schedules do |t|
      t.string :scheduleId
      t.string :scheduleName
      t.datetime :startDate
      t.datetime :endDate
      t.string :importFrom
      t.integer :exportCounts
      t.text :brief
      t.text :searchTag
      t.integer :status
      t.integer :browseCounts
      t.integer :likeCounts
      
      t.timestamps
    end
  end
end
