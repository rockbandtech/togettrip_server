class CreateJoinTableUserSchedule < ActiveRecord::Migration[5.1]
  def change
    create_join_table :users, :schedules do |t|
      t.index [:user_id, :schedule_id]
      t.index [:schedule_id, :user_id]
    end
  end
end
