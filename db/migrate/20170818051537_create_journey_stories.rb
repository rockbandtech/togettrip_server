class CreateJourneyStories < ActiveRecord::Migration[5.1]
  def change
    create_table :journey_stories do |t|
      t.string :storyId
      t.text :storyName
      t.text :opening
      t.text :bridge
      t.text :ending
      t.belongs_to :journey_assoc, :polymorphic => true

      t.timestamps
    end
  end
end
