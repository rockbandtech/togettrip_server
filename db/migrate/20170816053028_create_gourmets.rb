class CreateGourmets < ActiveRecord::Migration[5.1]
  def change
    create_table :gourmets do |t|
      t.time :openTime
      t.time :closeTime
      t.float :avgPrice
      t.integer :restaurantType
      t.integer :boxType
      t.boolean :creditCard
      t.text :spRequirements
      t.belongs_to :view_spot, foreign_key: true

      t.timestamps
    end
  end
end
