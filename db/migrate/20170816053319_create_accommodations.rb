class CreateAccommodations < ActiveRecord::Migration[5.1]
  def change
    create_table :accommodations do |t|
      t.integer :type
      t.float :stars
      t.boolean :wifiSupply
      t.integer :offerMeals
      t.integer :capacityLimit
      t.text :spRequirements
      t.belongs_to :view_spot, foreign_key: true

      t.timestamps
    end
  end
end
