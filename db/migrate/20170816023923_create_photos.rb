class CreatePhotos < ActiveRecord::Migration[5.1]
  def change
    create_table :photos do |t|
      t.string :image
      t.string :imageUrl
      t.string :fileName
      t.string :filePath
      t.text :cameraModel
      t.decimal :longitude
      t.decimal :latitude
      t.text :remarks
      t.datetime :filmingTime
      t.integer :fileType
      t.integer :category
      #t.integer :photo_assoc_id
      #t.string :photo_assoc_type
      t.belongs_to :photo_assoc, :polymorphic => true

      t.timestamps
    end
  end
end
