class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :userId
      t.integer :status
      t.string :email
      t.string :passportId
      t.string :accountName
      t.integer :accountType
      t.string :nation
      t.string :address
      t.string :avatarUrl
      t.datetime :birthday
      t.integer :gender
      t.string :cellPhone
      t.string :tel_home
      t.text :intorduction
      t.string :appKey
      t.string :token
      #改存Facebook userId
      t.string :pushToken

      t.timestamps
    end
  end
end
