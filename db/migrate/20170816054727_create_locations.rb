class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      t.integer :queueIndex
      t.string :spotId
      t.string :spotName
      t.decimal :longitude
      t.decimal :latitude
      t.time :arrivalTime
      t.time :leftTime
      t.integer :transportation
      t.text :story
      t.belongs_to :location_assoc, :polymorphic => true

      t.timestamps
    end
  end
end
