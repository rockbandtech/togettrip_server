class CreateViewSpots < ActiveRecord::Migration[5.1]
  def change
    create_table :view_spots do |t|
      t.string :spotId
      t.string :spotName
      t.string :country
      t.string :city
      t.string :town
      t.string :RdSt
      t.string :address
      t.decimal :longitude
      t.decimal :latitude
      t.integer :type
      t.text :description
      t.string :phone
      t.string :website
      t.string :hashtag
      
      t.timestamps
    end
  end
end
