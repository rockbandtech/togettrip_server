class CreateDailySchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :daily_schedules do |t|
      t.datetime :date
      t.integer :dayCounts
      t.text :diary
      # t.integer :daily_schdl_id
      # t.string :daily_schdl_type
      t.belongs_to :daily_schdl, :polymorphic => true

      t.timestamps
    end
  end
end
