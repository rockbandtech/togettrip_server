# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170820091552) do

  create_table "accommodations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "type"
    t.float "stars", limit: 24
    t.boolean "wifiSupply"
    t.integer "offerMeals"
    t.integer "capacityLimit"
    t.text "spRequirements"
    t.bigint "view_spot_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["view_spot_id"], name: "index_accommodations_on_view_spot_id"
  end

  create_table "daily_schedules", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "date"
    t.integer "dayCounts"
    t.text "diary"
    t.string "daily_schdl_type"
    t.bigint "daily_schdl_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["daily_schdl_type", "daily_schdl_id"], name: "index_daily_schedules_on_daily_schdl_type_and_daily_schdl_id"
  end

  create_table "gourmets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.time "openTime"
    t.time "closeTime"
    t.float "avgPrice", limit: 24
    t.integer "restaurantType"
    t.integer "boxType"
    t.boolean "creditCard"
    t.text "spRequirements"
    t.bigint "view_spot_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["view_spot_id"], name: "index_gourmets_on_view_spot_id"
  end

  create_table "journey_stories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "storyId"
    t.text "storyName"
    t.text "opening"
    t.text "bridge"
    t.text "ending"
    t.string "journey_assoc_type"
    t.bigint "journey_assoc_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["journey_assoc_type", "journey_assoc_id"], name: "index_journey_stories_on_journey_assoc_type_and_journey_assoc_id"
  end

  create_table "locations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "queueIndex"
    t.string "spotId"
    t.string "spotName"
    t.decimal "longitude", precision: 10
    t.decimal "latitude", precision: 10
    t.time "arrivalTime"
    t.time "leftTime"
    t.integer "transportation"
    t.text "story"
    t.string "location_assoc_type"
    t.bigint "location_assoc_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["location_assoc_type", "location_assoc_id"], name: "index_locations_on_location_assoc_type_and_location_assoc_id"
  end

  create_table "photos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "image"
    t.string "imageUrl"
    t.string "fileName"
    t.string "filePath"
    t.text "cameraModel"
    t.decimal "longitude", precision: 10
    t.decimal "latitude", precision: 10
    t.text "remarks"
    t.datetime "filmingTime"
    t.integer "fileType"
    t.integer "category"
    t.string "photo_assoc_type"
    t.bigint "photo_assoc_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["photo_assoc_type", "photo_assoc_id"], name: "index_photos_on_photo_assoc_type_and_photo_assoc_id"
  end

  create_table "schedules", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "scheduleId"
    t.string "scheduleName"
    t.datetime "startDate"
    t.datetime "endDate"
    t.string "importFrom"
    t.integer "exportCounts"
    t.text "brief"
    t.text "searchTag"
    t.integer "status"
    t.integer "browseCounts"
    t.integer "likeCounts"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedules_users", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id", null: false
    t.bigint "schedule_id", null: false
    t.index ["schedule_id", "user_id"], name: "index_schedules_users_on_schedule_id_and_user_id"
    t.index ["user_id", "schedule_id"], name: "index_schedules_users_on_user_id_and_schedule_id"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "userId"
    t.integer "status"
    t.string "email"
    t.string "passportId"
    t.string "accountName"
    t.integer "accountType"
    t.string "nation"
    t.string "address"
    t.string "avatarUrl"
    t.datetime "birthday"
    t.integer "gender"
    t.string "cellPhone"
    t.string "tel_home"
    t.text "intorduction"
    t.string "appKey"
    t.string "token"
    t.string "pushToken"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "view_spots", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "spotId"
    t.string "spotName"
    t.string "country"
    t.string "city"
    t.string "town"
    t.string "RdSt"
    t.string "address"
    t.decimal "longitude", precision: 10
    t.decimal "latitude", precision: 10
    t.integer "type"
    t.text "description"
    t.string "phone"
    t.string "website"
    t.string "hashtag"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "accommodations", "view_spots"
  add_foreign_key "gourmets", "view_spots"
end
