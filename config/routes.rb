Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope '/api' do
    # user api #
    post '/user/sign',    to: 'users#sign'
    post '/user/validate'   => "users#validateUser",  :as => "user_validate"
    post '/user/search',    to: 'users#searchUsers'
    put '/user/update',    to: 'users#userUpdate'
    
    # view spot api#
    post '/view_spots/search',    to: 'view_spots#searchViewSpots'
    post '/view_spots/nearby',    to: 'view_spots#searchNearbySpots'
    
    #schedule api#
    post '/schedule/create',    to: 'schedules#createSchedule'
    put '/schedule/update',    to: 'schedules#updateSchedule'
    post '/schedule/search',    to: 'schedules#searchSchedules'
    
    #journey story
    post '/story/create',    to: 'journey_stories#createNewStory'
    post '/story/create',    to: 'journey_stories#createNewStory'
    
    ###I just add this divac QAQ
    get 'test/send/mail'  => 'mail#send_simple_message'  ##1 OK but will delay 20 mins
    get 'test/valid/get'  => 'mail#get_valid'            ##2 OK store in front-end issue, expire issue
    get 'test/valid/verify' =>'mail#test_valid'          ##3 OK
    get 'test/send/valid'   => 'users#divacTest'         ##4 testing  integration of 1~2
    
    #shane test
    get 'shane' => 'shane#testapi'
  end

end