
require 'openssl'
class SchedulesController < ApplicationController
    
    skip_before_action :verify_authenticity_token
    
    def createSchedule
        userId_string = params[:userId]
        schedule_hash = {}
        @user = User.where(:userId => userId_string).first
        params[:newSchedule].each do |key,value|
            if !value.blank?
                schedule_hash[key] = value
            end
        end
        @schedule = Schedule.new(schedule_hash)
        @schedule.scheduleId = generateScheduleId()
        if !schedule_hash[:importFrom].blank?
            fromSCHDL = Schedule.where(:scheduleId => schedule_hash[:importFrom]).first
            fromSCHDL.exportCounts += 1
            fromSCHDL.save
        end
        @user.schedules.push(@schedule)
        respond_to do |format|
            if @user.save
                #flash[:notice] = 'ModelClassName was successfully created.'
                format.json {render :json =>{error:{errorCode: 0},schedule: filterEmtyData(@schedule.attributes)
                                            .as_json}.to_json}
            end
        end
    end
    
    def updateSchedule
        update_hash = {}
        update_hash = request_params.dup
        @schedule = Schedule.where(:scheduleId => update_hash[:scheduleId]).first
        update_hash.delete(:scheduleId)
        @schedule.update(update_hash)
        respond_to do |format|
            if @schedule.save
                format.json {render :json =>{error:{errorCode: 0},
                schedule: filterEmtyData(@schedule.attributes).as_json}.to_json}
            else
                format.json {render :json =>{error:{errorCode: 0}}.to_json}
            end
        end
    end
    
    def searchSchedules
        user = User.where(:userId => params[:userId]).first
        conditions_hash = {}
        conditions_hash = params[:conditions]
        results = []
        case conditions_hash[:searchType]
        when 'scheduleIds'
            scheduleIds_array = conditions_hash[:scheduleIds]
            scheduleIds_array.each_index do |index|
                schedule = Schedule.where(:scheduleId => scheduleIds_array[index]).first
                results.push(filterEmtyData(schedule.attributes))
            end
        when 'scheduleName'
            results = user.schedules.where("scheduleName LIKE ?","%#{conditions_hash[:scheduleName]}%")
                            .limit(conditions_hash[:limit]).offset(conditions_hash[:index])
                            .order(conditions_hash[:order])
        when 'dateDuration'
            results = user.schedules.where('created_at >= ? AND created_at <= ?',
                            conditions_hash[:startDate],conditions_hash[:endDate])
                            .limit(conditions_hash[:limit]).offset(conditions_hash[:index])
                            .order(conditions_hash[:order])
        end
        respond_to do |format|
            format.json {render :json => {error:{errorCode: 0},schedules: results}.to_json}
        end
        
    end
    def querySampleSchedules
        results = []
        scheduleIds_array = ['ScheduleIds']
        scheduleIds_array.each_index do |index|
            schedule = Schedule.where(:scheduleId => scheduleIds_array[index]).first
            results.push(filterEmtyData(schedule.attributes))
            end
    end
    ########
    private
    def request_params
        params.require(:schedule).permit!
    end
    def filterEmtyData(filter_hash)
        filter_hash.each do |key,value|
			if value.blank?
				filter_hash.delete(key)
			end
		end
		return filter_hash
    end
    def generateScheduleId
		head_string = "SCHDL:"
		return head_string + SecureRandom.hex(58)
    end
    
end
