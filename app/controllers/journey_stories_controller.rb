class JourneyStoriesController < ApplicationController
    
    def createNewStory
        
        userId_string = params[:userId]
        story_hash = {}
        @user = User.where(:userId => userId_string).first
        params[:newStory].each do |key,value|
            if !value.blank?
                story_hash[key] = value
            end
        end
        @newStory = JourneyStory.new(story_hash)
        @newStory.stroyId = generateStoryId()
        @user.journey_stories.push(@newStory)
        respond_to do |format|
            if @newStory.save
                format.json {render :json =>
                    {error:{errorCode: 0},stories: filterEmtyData(@newStory.attributes)
                    .as_json}
                    .to_json}
            end
        end
    end
    
    def searchStories
        conditions_hash = params[:conditions]
        case search_params[:searchType]
        when 'storyId'
            storyIds_array = conditions_hash[:spotIds]
            storyIds_array.each_index do |index|
                schedule = Schedule.where(:scheduleId => spotIds_array[index]).first
                results.push(filterEmtyData(schedule.attributes))
            end
        when 'storyName'
            results = user.schedules.where("storyName LIKE ?","%#{conditions_hash[:scheduleName]}%")
                            .limit(conditions_hash[:limit]).offset(conditions_hash[:index])
                            .order(conditions_hash[:order])
        when 'dateDuration'
        results = user.schedules.where('created_at >= ? AND created_at <= ?',
                            conditions_hash[:startDate],conditions_hash[:endDate])
                            .limit(conditions_hash[:limit]).offset(conditions_hash[:index])
                            .order(conditions_hash[:order])
        end
        respond_to do |format|
            format.json {render :json => {error:{errorCode: 0},stories: results}.to_json}
        end
    end
    
    private
    def filterEmtyData(filter_hash)
        filter_hash.each do |key,value|
			if value.blank?
				filter_hash.delete(key)
			end
		end
		return filter_hash
    end
    def generateStoryId
		head_string = "STORY:"
		return head_string + SecureRandom.hex(58)
    end
end
