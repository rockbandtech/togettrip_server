class MailController < ApplicationController
    def send_simple_message(msg)
      @domain = "sandbox7d5b97aeca554806b9ce7d09c1dcab06.mailgun.org"
      @apikey = "key-5ee4908364f566a91860ebaa58d4f0dd"
      
      
      RestClient.post "https://api:#{@apikey}"\
      "@api.mailgun.net/v3/#{@domain}/messages",
      :from => "Excited User <mailgun@#{@domain}>",
      :to => "divac.d.zoro@gmail.com",
      :subject => "Hello",
      :text => "Testing some Mailgun awesomness!"
      render json: %({a:1})
    end
    
    def get_valid
      o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
      string = (0...10).map { o[rand(o.length)] }.join
      session[:code] = string
      render json: %({code:"#{string}"})
    end
    
    def test_valid
      params.require(:code)
      render json: %({result:"#{session[:code]==params[:code]}"})
    end
end
