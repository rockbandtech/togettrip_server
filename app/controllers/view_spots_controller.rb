class ViewSpotsController < ApplicationController
    
    def searchViewSpots
        search_params = params.require(:conditions)
        conditions_hash = JSON.parse(search_params[:conditions])
        results = []
        case conditions_hash[:searchType]
        when 'spotIds'
            spotIds_array = conditions_hash[:spotIds]
            spotIds_array.each_index do |index|
                schedule = Schedule.where(:scheduleId => spotIds_array[index]).first
                results.push(filterEmtyData(schedule.attributes))
            end
        when 'spotName'
            results = ViewSpot.where("spotName LIKE ?","%#{conditions_hash[:spotName]}%")
                            .limit(conditions_hash[:limit])
                            .offset(conditions_hash[:index])
                            .order(conditions_hash[:order])
        when 'hashtag'
            results = ViewSpot.where("hashtag LIKE ?","%#{conditions_hash[:hashtag]}%")
                            .limit(conditions_hash[:limit])
                            .offset(conditions_hash[:index])
                            .order(conditions_hash[:order])
        end
        respond_to do |format|
            format.json {render :json => {error:{errorCode: 0},view_spots: results}.to_json}
        end
    end
    
    def searchNearbySpots
        
        geo_params = params.require(:geoLocation)
        geo_hash = JSON.parse(geo_params[:geoLocation])
        longitude = geo_hash[:longitude]
        latitude = geo_hash[:latitude]
        # SELECT id, ( 3959 * acos( cos( radians(37) ) * cos( radians( lat ) ) * 
        # cos( radians( lng ) - radians(geo_hash[:longitude]) ) + sin( radians(geo_hash[:latitude]) ) * 
        # sin( radians( lat ) ) ) ) AS distance FROM markers HAVING distance < 25 
        # ORDER BY distance LIMIT 0 , 20;
        
        results = []
        respond_to do |format|
            format.json {render :json => {error:{errorCode: 0},view_spots: results}.to_json}
        end
                                
    end
    ###################
    private
    
    def request_params
        params.require(:ViewSpot)
    end
    
    def filterEmptyData(filter_hash)
        filter_hash.each do |key,value|
            if value.blank?
                filter_hash.delete(key)
            end
        end
        return filter_hash
    end
end












