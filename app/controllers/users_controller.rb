
require 'openssl'
require 'digest'
require 'base64'
require 'securerandom'

class UsersController < ApplicationController
	
	skip_before_action :verify_authenticity_token
	
	def divacTest
		code = getVerification()
		render json: %({code:"#{code}"})
	end	
	
	def testFunction
		respond_to do |format|
			format.json {render :json => "隨便"}
		end
	end
	
	def sign
		# 使用者註冊與登入 #
		decrypt_email = aesDecrypt(sign_params[:email],sign_params[:appKey])
		@user = User.where(:email => decrypt_email).first
		respond_to do |format|
			if !@user.blank? && !sign_params[:token].blank?
				#社群登入
				
				if !isSameAppKey(sign_params[:appKey],@user.appKey)
					response_params = decryptParams(@user.attributes,@user.appKey)
					response_params[:appKey] = sign_params[:appKey]
					response_params = encryptParams(response_params,response_params[:appKey])
					updateUser(@user,response_params)
					response_params[:email] = sign_params[:email]
					#do kick
				end
				format.json {render :json =>{error:{errorCode: 0},userInfo: filterEmptyData(response_params).as_json}.to_json}
			
			elsif @user.blank? && !sign_params[:token].blank?
				#社群註冊
				create_hash = sign_params.dup
				create_hash[:email] = decrypt_email
				@user =  createNewUser(create_hash)
				@user.email = sign_params[:email]
				format.json  {render :json => {error:{errorCode: 0},userInfo: filterEmptyData(@user.attributes).as_json}.to_json}
			else
				##這邊要做寄信的動作，寄出後用回傳json通知Client端換頁
				verification_code = getVerification() #取得驗證碼
				subject_string = "Verification Code"  #信件主旨
				msg_string = "Your verification code is #{verification_code}" #信件內容
				sendMailTo(decrypt_email,subject_string,msg_string)#寄信
				format.json  {render :json => {error:{errorCode: 0},validateCode: verification_code}.to_json}
			end
		end
	end
	
	def validateUser
		#驗證使用者#
		respond_to do |format|
			if checkVerification()
				#驗證成功
				user_hash = sign_params.dup
				user_hash[:email] = aesDecrypt(sign_params[:email],sign_params[:appKey])
				
				@user = User.where(:email => user_hash[:email]).first
				if @user.blank?
					#新註冊
					user_hash = encryptParams(user_hash,sign_params[:appKey])
					@user = createNewUser(user_hash)
					@user.email = aesEncrypt(@user.email,@user.appKey)
					format.json {render :json => {error:{errorCode: 0},
								userInfo: filterEmptyData(@user.attributes).as_json
					}.to_json }
				else
					#重新登入
					response_params = {}
					if !isSameAppKey(sign_params[:appKey],@user.appKey)
						response_params = decryptParams(@user.attributes,@user.appKey)
						response_params[:appKey] = sign_params[:appKey]
						response_params = encryptParams(response_params,sign_params[:appKey])
						updateUser(@user,response_params)
					#do kick
					end
					response_params[:email] = aesEncrypt(sign_params[:email],sign_params[:appKey])
						format.json {render :json => {error:{errorCode: 0},
								userInfo: filterEmptyData(response_params).as_json
						}.to_json}
				end
			else
				#驗證失敗
				format.json {render :json => {error:{errorCode:1001,message:"驗證失敗"}
					}.to_json}
			end
		end
	end
	
	def userUpdate
		params.require(:user).except(:email)
		update_hash = params.dup
		@user = User.where(:userId => update_hash[:userId]).first
		#update_hash[:email] = aesDecrypt(params[:email],params[:appKey])
		
		@user = updateUser(@user,update_hash)
		respond_to do |format|
			if @user.blank?
				format.json {render :json => {error:{errorCode:1003,message:'update failed'}
					}.to_json}
			else
				format.json {render :json => {error:{errorCode: 0},
								userInfo: filterEmptyData(@user.attributes).as_json
					}.to_json }
			end
		end
		
	end
	
	def searchUsers
		userIds_params = params.require(:usersIds)
		userIds_array = userIds_params[:usersIds]
		results = []
		userIds_array.each_index do |index|
			user = User.where(:userId => userIds_array[index]).first
			if !user.blank?
				result_hash = user.attributes.dup
				#delete privacy data
				result_hash.reject! {|k, v| %w"id email passportId address birthday 
				cellPhone tel_home appKey intorduction token 
				pushToken created_at updated_at".include? k }
				results.push(filterEmtyData(result_hash))
			end
		end
		respond_to do |format|
			format.json {render :json => {error:{errorCode: 0},
								users: results
				}.to_json }
		end
	end
	
	private
	## params ##
	def sign_params
		params.require(:user)
		.permit(:email,:accountName,:accountType,:address,:avatarUrl,:birthday,:gender,:appKey,:token,:pushToken)
		#params.reject! {|k, v| %w"id userId created_at updated_at".include? k }
	end
	
	## CRUD ##
	def encryptParams(params_hash,publicKey)
		decrypt_hash = params_hash.dup
		decrypt_hash.reject! {|k, v| %w"id userId email accountType gender 
		intorduction appKey token pushToken created_at updated_at".include? k }
		decrypt_hash.each do |key,value|
			if !value.blank?
				params_hash[key] = aesEncrypt(value,publicKey)
			end
		end
		return params_hash
	end
	
	def decryptParams(params_hash,publicKey)
		encrypt_hash = params_hash.dup
		encrypt_hash.reject! {|k, v| %w"id userId email accountType createDate gender 
			intorduction appKey token pushToken created_at updated_at".include? k }
		encrypt_hash.each do |key,value|
			if !value.blank?
				params_hash[key] = aesDecrypt(value)
			end
		end
		return params_hash
	end
	def createNewUser (user_hash)
		
		@user = User.new(user_hash)
		@user.userId = generateUserId()
		if @user.save
			return @user
		else
			return nil
		end
	end
	
	def updateUser(user,update_hash)
		@user = user
		@user.update(update_hash)
		if @user.save
			return @user
		else
			return nil
		end
	end
	def filterEmptyData(user_hash)
		user_hash.each do |key,value|
			if value.blank?
				user_hash.delete(key)
			end
		end
		return user_hash
	end
	############ 驗證碼相關 
	def getVerification()
		random_str_set = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
    	code = (0...10).map { random_str_set[rand(random_str_set.length)] }.join
    	session[:validateCode] = code
    	return code
	end
	def checkVerification
		params.require(:validateCode)
    	return session[:validateCode]==params[:validateCode]
	end	
	############ 寄信 
	def sendMailTo(to_string,subject_string,msg_string)
    	domain = "sandbox7d5b97aeca554806b9ce7d09c1dcab06.mailgun.org"
    	apikey = "key-5ee4908364f566a91860ebaa58d4f0dd"
      
    	RestClient.post "https://api:#{apikey}"\
    	"@api.mailgun.net/v3/#{domain}/messages",
    	:from => "togetrip.no-reply<mailgun@#{domain}>",
    	:to => "divac.d.zoro@gmail.com,#{to_string}",
    	:subject => "#{subject_string}",
    	:text => "#{msg_string}"	
	end	
	##### Encrypt & Dencrypt  #######
	def generateUserId
		head_string = "MEM:"
		return head_string + SecureRandom.hex(28)
	end
	
	def isSameAppKey (newKey,oldKey)
		if newKey == oldKey
			return true
		else
			#do kick
			return false
		end
	end
	
	def aesEncrypt(input_string,publicKey)
		cipher = OpenSSL::Cipher::AES.new(256, :CBC)
		cipher.encrypt
		cipher.key = createAESPrivateKey(publicKey)
		#cipher.key = createAESPrivateKey("82eebe1161518e664856a02d546519bb2c32b11966a87a3397d45c1657b6511e840d5e7f7df52bde787ac11b0d8a09ae20d3c6b141fa8a966c01b542b7d6cd1c")
		encrypted = cipher.update(input_string) + cipher.final
		encrypted_base64 = Base64.encode64(encrypted)
		return encrypted_base64
	end
	def aesDecrypt(encrypt_string,publicKey)
		decipher = OpenSSL::Cipher::AES.new(256, :CBC)
		decipher.decrypt
		decipher.key = createAESPrivateKey(publicKey)
		decode_string = Base64.decode64(encrypt_string)
		plain = decipher.update(decode_string) + decipher.final
		return plain
	end
	
	# def createAESPublicKey(uuid_string)
	# 	time = Time.now.getutc
	# 	uuid_string += time
	# end
	
	def createAESPrivateKey(app_key_string)
		if app_key_string.blank? || app_key_string.length != 128
			return nil;
		end
		table_array = 
		[
			19,55,44,62,62,
			11,46,48,13,3,
			2,30,46,43,34,
			45,56,30,12,38,
			7,11,38,24,42,
			22,48,45,56,60,
			0,32
		];
		chars_array = []
		chars_array = app_key_string.split('')
		result_string = ""
		table_array.each do |position|
			result_string += chars_array[position]
		end
		return result_string
	end
	
end
