class Schedule < ApplicationRecord
    
    has_one :coverPhoto, :as => :photo_assoc
    has_many :stories, :as => :journey_assoc
    has_many :daily_shedules, :as => :daily_schdl,:dependent => :delete_all
    has_and_belongs_to_many :users,:dependent: :nullify
    
end
