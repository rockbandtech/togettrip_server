class Gourmet < ApplicationRecord
  
  belongs_to :view_spot
  has_many :menuPhotos, :as => :photo_assoc,:dependent => :delete_all
  has_many :recommendations, :as => :photo_assoc,:dependent => :delete_all
  
end
