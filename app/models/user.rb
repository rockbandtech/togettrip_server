class User < ApplicationRecord
    
    validates_presence_of :userId,:email
    has_and_belongs_to_many :schedules,dependent: :nullify
    has_many :favoriteSCHDLs
    has_many :stories, :as => :journey_assoc
end
