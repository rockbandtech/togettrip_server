class Location < ApplicationRecord
    
    validates_presence_of :spotName,:longitude,:latitude,:arrivalTime,:transportation
    belongs_to :location_assoc, :polymorphic => true
    has_one :locationPhoto, :as => :photo_assoc
    
end
