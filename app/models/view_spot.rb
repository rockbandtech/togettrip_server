class ViewSpot < ApplicationRecord
    
    validates_presence_of :spotId,:spotName,:longitude,:latitude
    has_one :gourmet
    has_one :accommodation
    has_many :photos, :as => :photo_assoc,:dependent => :delete_all
    
end
