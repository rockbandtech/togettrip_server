class DailySchedule < ApplicationRecord
    
    belongs_to :daily_schdl, :polymorphic => true
    has_many :locations, :as => :location_assoc,:dependent => :delete_all
    
end
