class Photo < ApplicationRecord
    
    belongs_to :photo_assoc, :polymorphic => true
    validates_presence_of :fileName
    
end
