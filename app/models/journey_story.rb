class JourneyStory < ApplicationRecord
  
  validates_presence_of :storyId,:scheduleId
  belongs_to :journey_assoc, :polymorphic => true
  has_many :pages, :as => :daily_schdl,:dependent => :delete_all
  
end
